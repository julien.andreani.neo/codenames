const express = require("express");
const app = express();
const server = require('http').createServer(app);
const socketio = require("socket.io");
const io = socketio(server);
const Game = require("./server/Game");
const port = process.env.PORT || 8080;

// Starting server
server.on("error", (err) => console.error("Server error : ", err));
server.listen(port, () => console.log(`Server started on port ${port}`));

// Routing
const clientPath = `${__dirname}/client`;
app.use(express.static(clientPath));

// Io
let game = new Game();

io.on("connection", (socket) => {
    socket.on("disconnect", () => {
        game.removePlayer(socket.id);
        game.onDisconnect(socket.id)
    });

    socket.id = Math.floor(Math.random() * 100);
    game.onConnect(socket);
    socket.emit("infos", game.getInfos());

    socket.on("addPlayer", data => game.addPlayer(socket.id, data.name));
    socket.on("playerSetTeam", data => game.playerSetTeam(socket.id, data.team));
    socket.on("playerSetRole", data => game.playerSetRole(socket.id, data.role));
    socket.on("startGame", () => game.start());
});

// setInterval(() => {

// }, 1000 / 25);