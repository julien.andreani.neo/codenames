class Player {
    static TEAM_BLUE;
    static TEAM_RED;
    static ROLE_SPY_MASTER;
    static ROLE_AGENT;

    constructor(_id, _name) {
        this.id = _id;
        this.name = _name;
        this.team = undefined;
        this.role = undefined;
    }
}

module.exports = Player;