const Player = require("./Player");

class Game {
    static BLUE_TURN;
    static RED_TURN;

    constructor() {
        this.started = false;
        this.turn = undefined;
        this.players = {};
        this.sockets = {};
    }

    start() {
        if (this.started)
            return;
        this.started = true;
    }

    playerSetTeam(id, team) {
        this.players[id].team = team;
    }

    playerSetRole(id, role) {
        this.players[id].role = role;
    }

    onConnect(socket) {
        this.sockets[socket.id] = socket;
    }

    onDisconnect(id) {
        delete this.sockets[id];
    }

    addPlayer(id, name) {
        this.players[id] = new Player(name);
    }

    removePlayer(id) {
        delete this.players[id];
    }

    getInfos() {
        return {
            teams: {
                blue: Player.TEAM_BLUE,
                red: Player.TEAM_RED
            },
            roles: {
                spyMaster: Player.ROLE_SPY_MASTER,
                agent: Player.ROLE_AGENT
            }
        };
    }
}

module.exports = Game;